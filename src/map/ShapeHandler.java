/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import java.util.ArrayList;
import javafx.scene.shape.Line;

/**
 *
 * @author vilmaparkkila
 */
public class ShapeHandler {
    ArrayList<Point> pointList = new ArrayList();
    ArrayList<Line> lineList = new ArrayList();
    static private ShapeHandler sh = null;
    
    private ShapeHandler() {
    
    }
    
    static public ShapeHandler getInstance() {
        if (sh == null)
            sh = new ShapeHandler();
        
        return sh;
    }
    
    public void addPoint(Point x) {
        pointList.add(x);
    }
    
    public void addLine(Line x) {
        lineList.add(x);
    }
    
    public ArrayList getPointList(){
        return pointList;
    }
}
