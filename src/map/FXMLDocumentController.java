/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;


/**
 *
 * @author vilmaparkkila
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private AnchorPane pane;
    
    Line line;
    double tempX;
    double tempY;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void onMouseClicked(MouseEvent event) {
        //Circle circle = new Circle(event.getSceneX(), event.getSceneY(),3);
        //pane.getChildren().add(circle);
        
        Point p = new Point(event.getSceneX(), event.getSceneY());
        pane.getChildren().add(p.getCircle());
        
        ShapeHandler sh = ShapeHandler.getInstance();
        sh.addPoint(p);
        
        //System.out.println(sh.getPointList());
        
        pane.getChildren().remove(line);
        
        if(sh.getPointList().size() > 1) {
            line = new Line(tempX, tempY, event.getSceneX(), event.getSceneY());
            pane.getChildren().add(line);
            sh.addLine(line);
        }
        
        tempX = event.getSceneX();
        tempY = event.getSceneY();
    }
    
}
