/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import javafx.scene.shape.Circle;

/**
 *
 * @author vilmaparkkila
 */
public class Point {
    private String name;
    private Circle circle;
    
    public Point(double x, double y) {

        name = "Piste";
        
        circle = new Circle(x, y, 3);
        
        System.out.println("Hei, olen piste!");

    }
    
    public Circle getCircle(){
        return circle;
    }
}
