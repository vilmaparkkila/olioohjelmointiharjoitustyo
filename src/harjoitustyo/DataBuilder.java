
package harjoitustyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author vilmaparkkila
 */
public class DataBuilder {
    private Document doc;
    
    private ArrayList<SmartPost> list = new ArrayList();

    public ArrayList<SmartPost> getList() {
        return list;
    }
    //gets data from xml file and puts it to doc
    public DataBuilder() {
        try {
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
        
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            String content = "";
            String line;

            while((line = br.readLine()) != null) {
                content += line + "\n";
            }
   
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            list = new ArrayList();
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //parses the xml data in doc, gets the wanted data out of it
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("place");
        
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            String city = getValue("city", e);
            String postoffice = getValue("postoffice", e);
            String availability = getValue("availability", e);
            String address = getValue("address", e);
            String lat = getValue("lat", e);
            String lng = getValue("lng", e);
            String code = getValue("code", e);
            
            list.add(new SmartPost(city, postoffice, availability, address, lat, lng, code));
        }
    }
    
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    //calculates distance using coordinates (in meters)
    public float calculateDistance(float lat1, float lng1, float lat2, float lng2) {
            double earthRadius = 6371000; //m
            double distLat = Math.toRadians(lat2-lat1);
            double distLng = Math.toRadians(lng2-lng1);
            double x = Math.sin(distLat/2) * Math.sin(distLat/2) +
                       Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                       Math.sin(distLng/2) * Math.sin(distLng/2);
            double y = 2 * Math.atan2(Math.sqrt(x), Math.sqrt(1-x));
            float distance = (float) (earthRadius * y);

            return distance;
    }
    
    //getters for the SmartPost data, using the SmartPost element
    public String getElementName(SmartPost e) {
        String name = e.getName();
        return name;
    }
    public String getPostOffice(SmartPost e) {
        String postOffice = e.getPostOffice();
        return postOffice;
    }
    public String getElementAvailability(SmartPost e) {
        String availability = e.getAvailability();
        return availability;
    }
    public String getElementAddress(SmartPost e) {
        String address = e.getAddress();
        return address;
    }
    public String getElementLat(SmartPost e) {
        String lat = e.getLat();
        return lat;
    } 
    public String getElementLng(SmartPost e) {
        String lng = e.getLng();
        return lng;
    }
    public String getElementCode(SmartPost e) {
        String lng = e.getCode();
        return lng;
    }
}
