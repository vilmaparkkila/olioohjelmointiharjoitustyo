
package harjoitustyo;

import java.util.ArrayList;

/**
 *
 * @author vilmaparkkila
 */

//PAKETEILLE koko ja painorajat
//1. class: max 5 kg, max 500 000 cm3, max 150km
//2. class: max 2 kg, max 100 000 cm3, can be breakable
//3. class: max 10 kg, max 1 000 000 cm3

public abstract class Package {
    protected Item item;
    protected ArrayList coordinates;
    protected String fCity;
    protected String tCity;
    protected float distance;
    
    public Package(Item i, ArrayList c, String fc, String tc, float d) {
        item = i;
        coordinates = c;
        fCity = fc;
        tCity = tc;
        distance = d;
    }
    
    public Item getItem() {
        return item;
    }
    public ArrayList getCoordinates() {
        return coordinates;
    }
    public String getFromCity() {
        return fCity;
    }
    public String getToCity() {
        return tCity;
    }
    public float getDist() {
        return distance;
    }

}
