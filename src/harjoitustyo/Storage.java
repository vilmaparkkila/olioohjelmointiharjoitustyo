
package harjoitustyo;

import java.util.ArrayList;

/**
 *
 * @author vilmaparkkila
 */
public class Storage {
    
    private ArrayList<Package> packageArray = new ArrayList();
    static private Storage s = null; // initializing Storage as SingletonClass
    private int numberOfPackages; //for the log
    
    private Storage() {
        
    }
    //gets instance of Storage
    static public Storage getInstance() {
        if (s == null)
            s = new Storage();
        
        return s;
    }
    //adds created new package to array
    public void addPackage(Package x) {
        packageArray.add(x);
        numberOfPackages++;
    }

    //getters
    public ArrayList getPackageArray(){
        return packageArray;
    }
    public int getNumberOfPackages(){
        return numberOfPackages;
    }
    public Item getItem(Package e) {
        Item i = e.getItem();
        return i;
    }
    public ArrayList getCoordinates(Package e) {
        ArrayList c = e.getCoordinates();
        return c;
    }
    
    
}
