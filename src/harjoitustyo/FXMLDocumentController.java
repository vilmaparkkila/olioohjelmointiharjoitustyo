
package harjoitustyo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.w3c.dom.Document;

/**
 *
 * @author vilmaparkkila
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private Button createPackageButton;
    @FXML
    private WebView view;
    @FXML
    private Button addToMapButton;
    @FXML
    private ComboBox smartPostComboBox;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button removePathsButton;
    @FXML
    private ComboBox packageComboBox;
    @FXML
    private Button refreshPackagesButton;
    @FXML
    private TextArea textArea;
    @FXML
    private Button refreshLogButton;
    
            
    private Document doc;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //loads the map
        view.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        //fills SmartPostcomboBox with cities
        DataBuilder db = new DataBuilder();
        Iterator iterator = db.getList().iterator();
        while (iterator.hasNext()) {
           Object element = iterator.next();
           String cityname = db.getElementName((SmartPost) element);
           if (!smartPostComboBox.getItems().contains(cityname)) {
               smartPostComboBox.getItems().add(cityname);
           }
        }
    }    

    @FXML
    private void addToMap(ActionEvent event) {
        //adds postoffices (and some details of it) to map using selected value from combobox
        String city = (String) smartPostComboBox.valueProperty().getValue();
        
        DataBuilder db = new DataBuilder();
        Iterator iterator = db.getList().iterator();

        while (iterator.hasNext()) {
            Object element = iterator.next();
            String name = db.getElementName((SmartPost) element);
            if (city.equals(name)) {
                String address = db.getElementAddress((SmartPost) element);
                String availability = db.getElementAvailability((SmartPost) element);
                String code = db.getElementCode((SmartPost) element);
                
                view.getEngine().executeScript("document.goToLocation('"+address+"), "+code+" "+city+"', '"+availability+"', 'blue')");
            }
        }   
    }
    //clears drawn paths from the map
    @FXML
    private void removePathsAction(ActionEvent event) {
        view.getEngine().executeScript("document.deletePaths()");
    }
    
    //opens new window, where you can create new package with item
    @FXML
    private void createPackageAction(ActionEvent event) {
        try {
            Stage packagewindow = new Stage();

            Parent page = FXMLLoader.load(getClass().getResource("FXMLCreatePackage.fxml"));

            Scene scene = new Scene(page);
            packagewindow.setScene(scene);
            scene.getStylesheets().add //initialize style sheet
            (FXMLDocumentController.class.getResource("cascadeStyleSheet.css").toExternalForm());
            packagewindow.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //draws path between starting city and destination city using script
    @FXML
    private void sendPackageAction(ActionEvent event) {
        Storage s = Storage.getInstance();
        ArrayList x = s.getCoordinates((Package) packageComboBox.getSelectionModel().getSelectedItem());
        view.getEngine().executeScript("document.createPath("+ x + ", 'red', 1)");
    }
    //fills packageComboBox with created packages
    @FXML
    private void refreshPackagesAction(ActionEvent event) {
        packageComboBox.getItems().clear();
        
        Storage s = Storage.getInstance();
        Iterator iterator = s.getPackageArray().iterator();
        while (iterator.hasNext()) {
           Object element = iterator.next();
           packageComboBox.getItems().add((Package) element);
        }
    } 

    //refreshes the log information
    @FXML
    private void refreshLogAction(ActionEvent event) {
        textArea.clear();
        
        Storage s = Storage.getInstance();
        int numOfPackages = s.getNumberOfPackages();
        textArea.setText(textArea.getText() + "Pakettien määrä: " + numOfPackages + "\n\n");
        
        Iterator iterator = s.getPackageArray().iterator();       
        while (iterator.hasNext()) {
            Object element = iterator.next();
            textArea.setText(textArea.getText() + "Paketin sisältö: "+ s.getItem((Package) element).getName() +
                    "\nLähtökaupunki: " + ((Package) element).getFromCity() +
                    "\nSaapumiskaupunki: " + ((Package) element).getToCity()  +
                    "\nMatkan pituus: " + Math.round((((Package) element).getDist())/100) + " km\n\n");
        }
        
        //prints log to file
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("logtext.txt"));

            out.write(textArea.getText());
            out.close();
        
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
