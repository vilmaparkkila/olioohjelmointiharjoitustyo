
package harjoitustyo;

/**
 *
 * @author vilmaparkkila
 */
public class Item {
    private String name;
    private double size;
    private double weight;
    private boolean breakable;
    
    public Item() {

    }
    
    public Item(String n, double s, double w, boolean b) {
        name = n;
        size = s;
        weight = w;
        breakable = b;
    }
    
    public String getName() {
        return name;
    }
    
    
}
