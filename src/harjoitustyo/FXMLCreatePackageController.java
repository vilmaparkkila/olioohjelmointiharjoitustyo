
package harjoitustyo;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author vilmaparkkila
 */
public class FXMLCreatePackageController implements Initializable {
    @FXML
    private ComboBox fromCity;
    @FXML
    private ComboBox fromPostOffice;
    @FXML
    private ComboBox toCity;
    @FXML
    private ComboBox toPostOffice;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createButton;
    @FXML
    private ComboBox itemsComboBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField weightField;
    @FXML
    private CheckBox breakableField;
    @FXML
    private Button infoButton;
    @FXML
    private ToggleGroup packageClass;
    @FXML
    private RadioButton firstClassBtn;
    @FXML
    private RadioButton secondClassBtn;
    @FXML
    private RadioButton thirdClassBtn;
    @FXML
    private Label alertLabel;
    
    private ArrayList<Float> al = new ArrayList<>();
    private ArrayList<Item> itemList = new ArrayList();
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //initializes startingpoint and destination comboBoxes with citynames
        DataBuilder db = new DataBuilder(); //city
        Iterator iterator = db.getList().iterator();

        while (iterator.hasNext()) {
           Object element = iterator.next();
           String cityname = db.getElementName((SmartPost) element);
           if (!fromCity.getItems().contains(cityname)) {
               fromCity.getItems().add(cityname);
           }
           if (!toCity.getItems().contains(cityname)) {
               toCity.getItems().add(cityname);
           }
        }
        //creates five initial items
        itemList.add(new Item("Puukauha", 200, 0.1, false));
        itemList.add(new Item("Aterinsetti", 7500, 0.7, false));
        itemList.add(new Item("Lasikulho", 80000, 0.5, true));
        itemList.add(new Item("Lautanen", 2700, 0.3, true));
        itemList.add(new Item("Valurautapannu", 25000, 1.0, false));
        
        //initializes itemsComboBox
        Iterator iterator2 = itemList.iterator();
        while (iterator2.hasNext()) {
           Object element = iterator2.next();
           Item x = (Item) element;
           itemsComboBox.getItems().add(x.getName());
        }
    }   
    //initializes startingpoint postoffice comboBox using selction of fromCity
    @FXML
    private void initializeFromPO(ActionEvent event) {
        fromPostOffice.getItems().clear();
        DataBuilder db = new DataBuilder();
        Iterator iterator = db.getList().iterator();
        while (iterator.hasNext()) {
           Object element = iterator.next();
           String city = db.getElementName((SmartPost) element);
           if (fromCity.getValue().equals(city)) {
               fromPostOffice.getItems().add(db.getPostOffice((SmartPost) element));
           }
        }
    }
    //initializes destination postoffice comboBox using selction of toCity
    @FXML
    private void initializeToPO(ActionEvent event) {
        toPostOffice.getItems().clear();
        DataBuilder db = new DataBuilder();
        Iterator iterator = db.getList().iterator();
        while (iterator.hasNext()) {
           Object element = iterator.next();
           String city = db.getElementName((SmartPost) element);
           if (toCity.getValue().equals(city)) {
               toPostOffice.getItems().add(db.getPostOffice((SmartPost) element));
           }
        }
    }

    //creates new package using initialized item or by users input
    @FXML
    private void createPackageAction(ActionEvent event) {
        
        Item i = new Item();
        int size = 0;
        double weight = 0;
        
        DataBuilder db = new DataBuilder();
        Iterator iterator = db.getList().iterator();

        //initializes coordinates and creates arraylist out of them
        String fLat = "";
        String fLng = ""; 
        String tLat = "";
        String tLng = "";       
        while (iterator.hasNext()) {
           Object element = iterator.next();
           if (fromPostOffice.getValue().equals(db.getPostOffice((SmartPost) element))) {
               fLat = db.getElementLat((SmartPost) element);
               fLng = db.getElementLng((SmartPost) element);
           } 
           if (toPostOffice.getValue().equals(db.getPostOffice((SmartPost) element))) {
               tLat = db.getElementLat((SmartPost) element);
               tLng = db.getElementLng((SmartPost) element);
           }
        }
        
        al.add(Float.parseFloat(fLat));
        al.add(Float.parseFloat(fLng));
        al.add(Float.parseFloat(tLat));
        al.add(Float.parseFloat(tLng));
        
        //calculates the distance between cities using function in DataBulder
        float dist = db.calculateDistance(Float.parseFloat(fLat), Float.parseFloat(fLng), Float.parseFloat(tLat), Float.parseFloat(tLng));
        
        if (!itemsComboBox.getSelectionModel().isEmpty()) { //initialized item selected
            Storage s = Storage.getInstance();
            Iterator iterator2 = itemList.iterator();
            while (iterator2.hasNext()) {
               Object element = iterator2.next();
               Item x = (Item) element;
               if (itemsComboBox.getValue() == x.getName()) {
                   i = x;
               }
            }
            
            //sets class value if initialized item is selected
            if (itemsComboBox.getValue() == "Puukauha") {
                firstClassBtn.setSelected(true);
            } else if (itemsComboBox.getValue() == "Aterinsetti") {
                firstClassBtn.setSelected(true);
            } else if (itemsComboBox.getValue() == "Lasikulho") {
                secondClassBtn.setSelected(true);
            } else if (itemsComboBox.getValue() == "Lautanen") {
                secondClassBtn.setSelected(true);
            } else if (itemsComboBox.getValue() == "Valurautapannu") {
                thirdClassBtn.setSelected(true);
            }
            
        } else { //user creates new item
            String str = sizeField.getText();
            String[] parts = str.split("\\*");
            int cm1 = Integer.parseInt(parts[0]); 
            int cm2 = Integer.parseInt(parts[1]); 
            int cm3 = Integer.parseInt(parts[2]); 
            size = cm1*cm2*cm3;
            
            System.out.println(cm1);
            System.out.println(cm2);
            System.out.println(cm3);
            System.out.println(size);
            
            String name = nameField.getText();
            weight = Double.parseDouble(weightField.getText());
            boolean breakable = breakableField.isSelected();
            
            i = new Item(name, size, weight, breakable);
  
        }

        // creating 1, 2 or 3. class package based on radiobutton selection
        // makes comparisons if package is right size etc. and alerts if not -> package cannot be made
        Storage s = Storage.getInstance();    
        if (firstClassBtn.isSelected()) {
            if (size > 300000) {
                alertLabel.setText("Liian suuri 1.lk paketiksi.");
            } else if (weight > 5) {
                alertLabel.setText("Liian painava 1.lk paketiksi.");
            } else if (dist > 150000) {
                alertLabel.setText("1.lk paketti voidaan \nkuljettaa vain 150km päähän.");
            } else if (breakableField.isSelected()) {
                alertLabel.setText("Särkyvä esine tulee \nlähettää 2.lk pakettina.");
            } else {
                s.addPackage(new FirstClass(i, al, (String) fromCity.getValue(), (String) toCity.getValue(), dist));
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();
            }
        } else if (secondClassBtn.isSelected()) {
            if (size > 100000) {
                alertLabel.setText("Liian suuri 2.lk paketiksi.");
            } else if (weight > 2) {
                alertLabel.setText("Liian painava 2.lk paketiksi.");
            } else {
                s.addPackage(new SecondClass(i, al, (String) fromCity.getValue(), (String) toCity.getValue(), dist));
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();
            }
        } else if (thirdClassBtn.isSelected()) {
            if (size > 500000) {
                alertLabel.setText("Liian suuri 3.lk paketiksi.");
            } else if (weight > 10) {
                alertLabel.setText("Liian painava 3.lk paketiksi.");
            } else if (breakableField.isSelected()) {
                alertLabel.setText("Särkyvä esine tulee \nlähettää 2.lk pakettina.");
            } else {    
                s.addPackage(new ThirdClass(i, al, (String) fromCity.getValue(), (String) toCity.getValue(), dist));
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();
            }
        } else {
            alertLabel.setText("Valitse pakettiluokka");
        }

    }
    
    //calcels the package creation if cancelButton pressed
    @FXML
    private void cancelCreatePackageAction(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    //shows info in alert window when infoButton pressed
    @FXML
    private void getInfoAction(ActionEvent event) {
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Tietoa pakettiluokista");
        alert.setHeaderText("PAKETTILUOKAT");
        alert.setContentText("1.lk paketti: Nopein, Maksimikuljetusetäisyys 150km, Ei särkyville tavaroille\n"
                + "Maksimipaino: 5kg, Maksimitilavuus: 300 000 cm3\n\n"
                + "2.lk paketti: Turvakuljetus, Särkyville tavaroille, Pitkät kuljetusetäisyydet, Paketti esineen kokoa vastaava\n"
                + "Maksimipaino: 2kg, Maksimitilavuus: 100 000 cm3\n\n"
                + "3.lk paketti: Hitain, Tavaran oltava kestävä ja suuri koko/paino eduksi, Ei särkyville tavaroille\n"
                + "Maksimipaino: 10kg, Maksimitilavuus: 500 000 cm3");
        alert.showAndWait();
    }   

}
