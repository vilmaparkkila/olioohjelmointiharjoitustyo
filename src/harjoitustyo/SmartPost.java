
package harjoitustyo;

/**
 *
 * @author vilmaparkkila
 */
public class SmartPost {
    private String city;
    private String postoffice;
    private String availability;
    private String address;
    private String latitude;
    private String longditude;
    private String code;
    
    public SmartPost(String c, String po, String av, String ad, String lat, String lng, String co) {
        city = c;
        postoffice = po;
        availability = av;
        address = ad;
        latitude = lat;
        longditude = lng;
        code = co;
    }
    
    public String getName() {
        return city;
    }
    public String getPostOffice() {
        return postoffice;
    }
    public String getAvailability() {
        return availability;
    }
    public String getAddress() {
        return address;
    }
    public String getLat() {
        return latitude;
    }
    public String getLng() {
        return longditude;
    }
    public String getCode() {
        return code;
    }
}
