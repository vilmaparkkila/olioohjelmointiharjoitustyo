/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;


/**
 *
 * @author vilmaparkkila
 */
public abstract class Account {
    private String accountNumber;
    private int amountOfMoney;
    
    
    
    public Account(String num, int money) {
        accountNumber = num;
        amountOfMoney = money;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
    
    public double getAmountOfMoney() {
        return amountOfMoney;
    }
    
}

class OrdinaryAccount extends Account {

    public OrdinaryAccount(String num, int money) {
        super(num, money);
    }

}
