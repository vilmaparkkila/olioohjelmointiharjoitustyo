/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;

import java.util.ArrayList;

/**
 *
 * @author vilmaparkkila
 */
public class Bank {
    
    private String accountNumber;
    private int money;
    ArrayList<Account> account_array = new ArrayList();
    
    public Bank() {
    
    }
    
    
    public void addAccount(String num, int amount) {
        account_array.add(new OrdinaryAccount(num, amount));
        System.out.println("Tili luotu.");
    }
    
    public void addCreditAccount(String num, int amount, int cr) {
        System.out.println("Pankkiin lisätään: " + num + "," + amount + "," + cr);
    }
    
    public void removeAccount(String num) {
        int x = account_array.indexOf(num);
        account_array.remove(x-1);
        System.out.println("Tili poistettu.");
    }
    
    public void moneyIn(String num, int amount) {  
        int x = account_array.indexOf(num);
        int y = (int)account_array.get(x).getAmountOfMoney();
        int z = y + amount;
        account_array.remove(x-1);
        account_array.add(new OrdinaryAccount(num, amount));
    }
    
    public void moneyOut(String num, int amount) {
        int x = account_array.indexOf(num);
        int y = (int)account_array.get(x).getAmountOfMoney();
        int z = y - amount;
        account_array.remove(x-1);
        account_array.add(new OrdinaryAccount(num, amount));
    }
    
    public void searchAccount(String num) {
        System.out.println("Etsitään tiliä: " + num);
    }
    
    public void printAccount(String num) {
        int x = account_array.indexOf(num);
        int y = (int)account_array.get(x).getAmountOfMoney();
        System.out.println("Tilinumero: "+ num +" Tilillä rahaa: " + y);
    }
    
    public void printAccounts() {
        System.out.println("Kaikki tilit:");
    }
    
    
    
}