/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;

import java.util.ArrayList;

/**
 *
 * @author vilmaparkkila
 */
public class BottleDispenser {
    
    private int bottles;
    private double money;
    ArrayList<Bottle> bottle_array = new ArrayList();
    
    public BottleDispenser() {
        bottles = 5;
        money = 0;
        
        bottle_array.add(new Bottle());
        bottle_array.add(new Bottle("Pepsi Max", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int x) {
        if (bottles < 0) {
            System.out.println("Pullojen määrä negatiivinen");
        }
        else if (money < 1 || money < bottle_array.get(x-1).getPrice()) {
            System.out.println("Syötä rahaa ensin!");   
        }
        else {
            System.out.println("KACHUNK! " + bottle_array.get(x-1).getName() + " tipahti masiinasta!");
            money -= bottle_array.get(x-1).getPrice();
            bottle_array.remove(x-1);
            bottles -= 1;
        }
    }
    
    public void returnMoney(double x) {
        double temp = Math.round(x * 100);
        x = (temp/100); 
        
        String m = String.format("%.2f", x );
        m = m.replaceAll("\\.",",");
       
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + m + "€");
        money = 0;
    }
    
    public void printBottles() {
        for (int i=0; i <= bottles; ++i) {
            
            Bottle b = new Bottle();
            
            System.out.println(i+1 + ". Nimi: " + bottle_array.get(i).getName());
            System.out.println("	Koko: " + bottle_array.get(i).getSize() + "	Hinta: " + bottle_array.get(i).getPrice());
        }
    }
    
    
    public Bottle getBottle(int i) {
        return bottle_array.get(i);
    }
    
    public double getMoney() {
        return money;
    }
}