/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author vilmaparkkila
 */
public class IOoperator {
    private String filename;
     
    public IOoperator (String s) {
        filename = s;
    }
    
    public void readZip() throws FileNotFoundException, IOException {
        
        ZipFile zf = new ZipFile(filename);
        Enumeration entries = zf.entries();
        
        
        
        while (entries.hasMoreElements()) {
            ZipEntry ze = (ZipEntry) entries.nextElement();
            InputStream stream = zf.getInputStream(ze);
            
            BufferedReader br = new BufferedReader(new InputStreamReader(zf.getInputStream(ze)));
            
            String text;
            while ((text = br.readLine()) != null) {
                System.out.println(text);
            }
            br.close();
        }
    }
    
}
