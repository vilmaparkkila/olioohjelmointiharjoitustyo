/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;

/**
 *
 * @author vilmaparkkila
 */
public class Car {
    
    public Car() {
        Body b = new Body();
        Chassis c = new Chassis();
        Engine e = new Engine();
        Wheel w = new Wheel();
        Wheel w2 = new Wheel();
        Wheel w3 = new Wheel();
        Wheel w4 = new Wheel();
        
        System.out.println("Autoon kuuluu:\n\t" 
         + b.getClass().getSimpleName() + "\n\t"
         + c.getClass().getSimpleName() + "\n\t"
         + e.getClass().getSimpleName() + "\n\t4 "
         + w.getClass().getSimpleName());
    }
}


class Body {
    Body() {
        System.out.println("Valmistetaan: Body");
    }
}

class Chassis {
    Chassis() {
        System.out.println("Valmistetaan: Chassis");
    }
}

class Engine {
    Engine() {
        System.out.println("Valmistetaan: Engine");
    }
}

class Wheel {
    Wheel() {
        System.out.println("Valmistetaan: Wheel");    
    }
}


