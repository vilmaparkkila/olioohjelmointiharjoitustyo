/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;

/**
 *
 * @author vilmaparkkila
 */
public class Bottle {
    private String name;
    private String manufacturer;
    private double total_energy;
    private double size;
    private double price;
    
    
    public Bottle() {
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        size = 0.5;
        price = 1.80;
    }
    
    public Bottle(String n, double s, double p) {
        name = n;
        size = s;
        price = p;
    }
    
    public String getName() {
        return name;
    }
    
    public double getSize() {
        return size;
    }
    
    public double getPrice() {
        return price;
    }
}