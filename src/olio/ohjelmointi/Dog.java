/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;

/**
 *
 * @author vilmaparkkila
 */
public class Dog {
    private String name;
    private String says;
    
    public Dog() {
        
    }
    
    public Dog(String x) {
        if (x.trim().isEmpty()) {
            name = "Doge";
            System.out.println("Hei, nimeni on " + name);
        }
        else {
            name = x;
            System.out.println("Hei, nimeni on " + name);
        }
        
    }
    
    public void speak() {
        
    }
    
    public void speak(String y, String z) {
        if (y.trim().isEmpty()) {
            says = "Much wow!";
            System.out.println(name + ": " + says);
        }
        else {
            System.out.println("Such " + z + ": " + y);
        }
    }
}
