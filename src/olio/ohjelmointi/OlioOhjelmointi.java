/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author vilmaparkkila
 */
public class OlioOhjelmointi {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */

    public static void main(String[] args) throws IOException {
        int choice;
        String accountNumber = "";
        int money = 0;
	int credit = 0;
        
        Scanner scan = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        Bank b = new Bank();
        
        do{
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***\n" +
                                "1) Lisää tavallinen tili\n" +
                                "2) Lisää luotollinen tili\n" +
                                "3) Tallenna tilille rahaa\n" +
                                "4) Nosta tililtä\n" +
                                "5) Poista tili\n" +
                                "6) Tulosta tili\n" +
                                "7) Tulosta kaikki tilit\n" +
                                "0) Lopeta");
            System.out.print("Valintasi: ");
            choice = scan.nextInt();
            
            switch (choice) {
                case 1:
                    System.out.print("Syötä tilinumero: ");
                    accountNumber = br.readLine();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    b.addAccount(accountNumber, money);
                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    accountNumber = br.readLine();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    System.out.print("Syötä luottoraja: ");
                    credit = scan.nextInt();
                    b.addCreditAccount(accountNumber, money, credit);
                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    accountNumber = br.readLine();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    b.moneyIn(accountNumber, money);
                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    accountNumber = br.readLine();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    b.moneyOut(accountNumber, money);
                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");
                    accountNumber = br.readLine();
                    b.removeAccount(accountNumber);
                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    accountNumber = br.readLine();
                    b.printAccount(accountNumber);
                    break;
                case 7:
                    b.printAccounts();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
                    break;
              }
        } while(choice != 0);
        
        
    }
}
