/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texteditor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


/**
 *
 * @author vilmaparkkila
 */
public class FXMLDocumentController implements Initializable {
    

    @FXML
    private Button loadButton;
    @FXML
    private Button saveButton;
    @FXML
    private TextArea textArea;   
    @FXML
    private TextField fileName;
    
    private String name;
    
    @FXML
    private void loadButtonAction(ActionEvent event) throws FileNotFoundException, IOException {
        name = fileName.getText();
        fileName.setText("");
        
        textArea.setText("");
        
        try {
            BufferedReader in = new BufferedReader(new FileReader(name));

            for (String text; (text = in.readLine()) != null;) {
                    textArea.setText(textArea.getText() + text + "\n");
            }
            
            in.close();
        }
        
        catch (IOException ex) {
            System.err.println (ex.toString());
            System.err.println("Could not find file " + fileName);
        }
        
        
    }

    @FXML
    private void saveButtonAction(ActionEvent event) throws IOException {
        name = fileName.getText();
        fileName.setText("");
        
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(name));
            out.write(textArea.getText());
            
            out.close();
        }
        catch (IOException ex) {
            System.err.println (ex.toString());
            System.err.println("Could not find file " + fileName);
        }
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    

}
