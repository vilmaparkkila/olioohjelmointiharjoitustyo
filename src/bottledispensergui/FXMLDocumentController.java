/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bottledispensergui;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import olio.ohjelmointi.BottleDispenser;

/**
 *
 * @author vilmaparkkila
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button addMoneyButton;
    @FXML
    private Button returnMoneyButton;
    @FXML
    private Label textArea;
    @FXML
    private Button buyBottleButton;
    @FXML
    private Label label;
    @FXML
    private ComboBox comboBottle;
    @FXML
    private ComboBox comboSize;
    @FXML
    private Slider moneySlider;
    @FXML
    private Button printButton;
    
    private double money;
    private String text;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BottleDispenser bd = null;
        bd = BottleDispenser.getInstance();
        
        comboBottle.getItems().add("Pepsi Max");
        comboBottle.getItems().add("Coca-Cola Zero");
        comboBottle.getItems().add("Fanta Zero");
        comboSize.getItems().add(0.5);
        comboSize.getItems().add(1.5);
    }    

    @FXML
    private void addMoney(ActionEvent event) {
        moneySlider.setValue(0);
        
        BottleDispenser bd = BottleDispenser.getInstance();
        bd.addMoney(money);
        
        textArea.setText("Klink! Laitteeseen lisättiin " + money + "€!");
    }

    @FXML
    private void returnMoney(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        double moneyback = bd.getMoney();
        String m = bd.returnMoney(moneyback);
        
        textArea.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + m + "€");
    }

    @FXML
    private void buyBottle(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();        
        text = bd.buyBottle((String) comboBottle.valueProperty().getValue(), (double) comboSize.valueProperty().getValue());
        
        textArea.setText(text);
    }

    @FXML
    private void sliderChange(MouseEvent event) {    
        money = moneySlider.getValue();
    }

    @FXML
    private void printReceiptAction(ActionEvent event) throws IOException {
        BottleDispenser bd = BottleDispenser.getInstance();
        BufferedWriter out = new BufferedWriter(new FileWriter("kuitti.txt"));
        out.write("PULLONPALAUTUSKUITTI\n\n"
                + "Tuotteen nimi: " + bd.getName() + "\n"
                + "Tuotteen hinta: " + bd.getPrice() + "€\n");

        out.close();
        textArea.setText("Kuitti tulostettu!");
    }
    
}
