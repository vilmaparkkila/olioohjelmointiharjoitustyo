/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author vilmaparkkila
 */
public class Areas {
    private Document doc;
    
    ArrayList<Theatre> arealist = new ArrayList();

    public ArrayList<Theatre> getList() {
        return arealist;
    }
    
    
    public Areas() {
        try {
            URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
        
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            String content = "";
            String line;

            while((line = br.readLine()) != null) {
                content += line + "\n";
            }
            
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            arealist = new ArrayList();
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Areas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            arealist.add(new Theatre(getValue("Name", e), getValue("ID", e)));
        }
    }
    
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    
    }
    
    public String getElementName(Theatre e) {
        String name = e.getName();
        return name;
    }
    
    public String getElementID(Theatre e) {
        String id = e.getID();
        return id;
    }
}
