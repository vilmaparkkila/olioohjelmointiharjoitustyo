/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

/**
 *
 * @author vilmaparkkila
 */
public class Theatre {
    private String name;
    private String id;
    
    
    public Theatre(String a, String i) {
        name = a;
        id = i;
    }
    
    public String getName() {
        return name;
    }
    
    public String getID() {
        return id;
    }
}
