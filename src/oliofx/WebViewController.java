/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebView;
import javax.script.ScriptException;

/**
 * FXML Controller class
 *
 * @author vilmaparkkila
 */
public class WebViewController implements Initializable {
    @FXML
    private Button previousPageButton;
    @FXML
    private Button nextPageButton;
    @FXML
    private Button refreshButton;
    @FXML
    private TextField urlInput;
    @FXML
    private WebView webViewArea;
    @FXML
    private Button indexButton;
    @FXML
    private Button initialaizeButton;
    
    //private String previouspage = "";
    //private String nextpage = "";
    ArrayList<String> pArray = new ArrayList<>(10);
    ArrayList<String> nArray = new ArrayList<>(10);
    
    Object temp = "";

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        urlInput.setText("www.google.com");
        webViewArea.getEngine().load("http://" + urlInput.getText());
        //previouspage = urlInput.getText(); 
        pArray.add(urlInput.getText());
        temp = urlInput.getText();
    }  

    @FXML
    private void refreshAction(ActionEvent event) {
        webViewArea.getEngine().load("http://" + urlInput.getText());
        
    }

    @FXML
    private void OnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            if (urlInput.getText().equals("index.html")) {
                webViewArea.getEngine().load(getClass().getResource("index.html").toExternalForm());
                urlInput.setText("index.html");
            } else {
                webViewArea.getEngine().load("http://" + urlInput.getText());
            }
            
            if (pArray.size() > 10) {
                temp = urlInput.getText();
                ListIterator litr = pArray.listIterator();
                while (litr.hasNext()) {
                    litr.set(temp);
                    temp = litr.next();
                }
            } else {
                pArray.add(0, urlInput.getText());
            }
        }
    }

    @FXML
    private void indexPageAction(ActionEvent event) throws ScriptException {
        webViewArea.getEngine().executeScript("document.shoutOut()");
    }
    
    @FXML
    private void initializeAction(ActionEvent event) {
        webViewArea.getEngine().executeScript("initialize()");
    }
    

    @FXML
    private void previousPageAction(ActionEvent event) {
        //nextpage = urlInput.getText();
        //webViewArea.getEngine().load("http://" + previouspage);
        //urlInput.setText(previouspage);
        
        webViewArea.getEngine().load("http://" + pArray.get(0));
        
        if (nArray.size() > 10) {
            ListIterator litr = nArray.listIterator();
            while (litr.hasNext()) {
                litr.set(temp);
                temp = litr.next();
            }
        } else {
            nArray.add(0, urlInput.getText());
        }
        urlInput.setText(pArray.get(0));
        pArray.remove(0);
        temp = urlInput.getText();
        
    }

    @FXML
    private void nextPageAction(ActionEvent event) {
        //previouspage = urlInput.getText();
        //webViewArea.getEngine().load("http://" + nextpage);
        //urlInput.setText(nextpage);
        webViewArea.getEngine().load("http://" + nArray.get(0));  
        
        if (pArray.size() > 10) {
            ListIterator litr = pArray.listIterator();
            while (litr.hasNext()) {
                litr.set(temp);
                temp = litr.next();
            }
        } else {
            pArray.add(0, urlInput.getText());
        }
        urlInput.setText(nArray.get(0));
        nArray.remove(0);
        temp = urlInput.getText();
        

    }


    
}
