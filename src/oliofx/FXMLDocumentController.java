/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author vilmaparkkila
 */
public class FXMLDocumentController implements Initializable{
    @FXML
    private ComboBox comboBox;
    @FXML
    private TextField startingTimeInput;
    @FXML
    private TextField dateInput;
    @FXML
    private TextField endingTimeInput;
    @FXML
    private Button listMoviesButton;
    @FXML
    private Button searchButton;
    @FXML
    private TextField movieNameInput;
    @FXML
    private ListView<String> listView = new ListView<String>();
    
    String id;
    private Document doc;
    
    ObservableList<String> observableList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Areas a = new Areas();
        Iterator iterator = a.getList().iterator();

        while (iterator.hasNext()) {
           Object element = iterator.next();
           String name = a.getElementName((Theatre) element);
           comboBox.getItems().add(name);
        }
        
    }    

    @FXML
    private void listMoviesAction(ActionEvent event) throws ParseException {
        
        observableList.remove(0, observableList.size());
        
        String date = dateInput.getText();
        
        if (date.isEmpty()) {
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date ddate = new Date();
            date = dateFormat.format(ddate);
        }

        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");

        Areas a = new Areas();
        Iterator iterator = a.getList().iterator();

        while (iterator.hasNext()) {
           Object element = iterator.next();
           String name = (String) comboBox.valueProperty().getValue();
           if (name.equals(a.getElementName((Theatre) element))) {
               //System.out.println(name);
               id = a.getElementID((Theatre) element);
               //System.out.println(id);
           }
        }
        
        //System.out.println(date);
        
        try {
            URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + date);
            
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            String content = "";
            String line;

            while((line = br.readLine()) != null) {
                content += line + "\n";
            }

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
           
            doc.getDocumentElement().normalize();
            
            NodeList nodes = doc.getElementsByTagName("Show");
        
            for(int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                Element e = (Element) node;
                
                String start = ((Element)e.getElementsByTagName("dttmShowStart").item(0)).getTextContent();
                String end = ((Element)e.getElementsByTagName("dttmShowEnd").item(0)).getTextContent();
                
                Date st = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(start);
                String startingtime = new SimpleDateFormat("HH:mm").format(st);
                
                Date ed = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(end);
                String endingtime = new SimpleDateFormat("HH:mm").format(ed); 
                
                if (startingTimeInput.getText().isEmpty() || endingTimeInput.getText().isEmpty()) {
                    String newitem = ((Element)e.getElementsByTagName("Title").item(0)).getTextContent() + ": " + startingtime + "-" + endingtime;
                    //System.out.println(newitem);
                    observableList.add(newitem);
                }
                else {
                    Date sTime = parser.parse(startingTimeInput.getText());
                    Date eTime = parser.parse(endingTimeInput.getText());
                    
                    Date sDate = parser.parse(startingtime);
                    Date eDate = parser.parse(endingtime);
                    
                    if (sDate.after(sTime) && eDate.before(eTime) && eDate.after(sTime)) {
                        String newitem = ((Element)e.getElementsByTagName("Title").item(0)).getTextContent() + ": " + startingtime + "-" + endingtime;
                        //System.out.println(newitem);
                        observableList.add(newitem);
                    }
                }
            }
            
            listView.setItems(observableList);
            
            startingTimeInput.setText("");
            endingTimeInput.setText("");
            dateInput.setText("");
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Areas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    @FXML
    private void listByNameAction(ActionEvent event)  {
        
        observableList.remove(0, observableList.size());
        
        String movie = movieNameInput.getText();
        movieNameInput.setText("");
        
        observableList.add(movie + ":");
        observableList.add(" ");
        
        try {
            URL url = new URL("http://www.finnkino.fi/xml/Schedule/");
            
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            String content = "";
            String line;

            while((line = br.readLine()) != null) {
                content += line + "\n";
            }

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
           
            doc.getDocumentElement().normalize();
            
            NodeList nodes = doc.getElementsByTagName("Show");
        
            for(int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                Element e = (Element) node;
                
                
                String name = ((Element)e.getElementsByTagName("Title").item(0)).getTextContent();
                if (movie.equals(name)){

                    String start = ((Element)e.getElementsByTagName("dttmShowStart").item(0)).getTextContent();
                    String end = ((Element)e.getElementsByTagName("dttmShowEnd").item(0)).getTextContent();

                    Date st = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(start);
                    String startingtime = new SimpleDateFormat("HH:mm").format(st);

                    Date ed = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(end);
                    String endingtime = new SimpleDateFormat("HH:mm").format(ed);

                    String newitem = ((Element)e.getElementsByTagName("Theatre").item(0)).getTextContent() + ": " + startingtime + "-" + endingtime;
                    //System.out.println(newitem);
                    observableList.add(newitem);
                    
                }
            }
            //System.out.println(observableList);
            listView.setItems(observableList);
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Areas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

    
    
}